package com.example.student.snapchat;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogIn extends AppCompatActivity {


    public LogIn() {
        // Required empty public constructor
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        LogInFragment login = new LogInFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.LogInContainer, login).commit();

    }
}
