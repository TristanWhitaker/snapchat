package com.example.student.snapchat;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        String[] loginmenuItems = {"Register", "Log In"};
        Button register = (Button) view.findViewById(R.id.Register);
        Button login = (Button) view.findViewById(R.id.LogIn);

                ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.simple_list_item_1,
                        loginmenuItems
        );

       // listView.setAdapter(listViewAdapter);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Register.class);
                startActivity(intent);
            }


        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LogIn.class);
                startActivity(intent);
            }


        });


        /*@Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {

            } else if (position == 1) {
                Intent intent = new Intent(getActivity(), LogIn.class);
                startActivity(intent);
                //Toast.makeText(getActivity(), "You clicked the second item", Toast.LENGTH_SHORT).show();
            }
        }
    });*/
        // Inflate the layout for this fragment
        return view;

    }

}
